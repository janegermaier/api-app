var request = require('request');
var chai = require('chai');
const { isMainThread } = require('worker_threads');
var expect = chai.expect

var urlBase = 'http://127.0.0.1:8000/'

describe('Request test', function(){
    it('Response 200', function(done){
        request(urlBase, function(error, response){
            console.log(error)
            console.log(response.statusCode)
            expect(response.statusCode).to.equal(200);
        });
        done();
    });
});