from fastapi.testclient import TestClient
from api.main import app

client = TestClient(app)


def test_read_root():
    """Load main page"""
    response = client.get("/")
    assert response.status_code == 200


def test_create_file():
    """Test upload file"""
    response = client.post("/files/upload",
                           files={'file': ('jokes.json', open('api/json/jokes.json', 'rb'), "multipart/form-data")}
                           )
    assert response.status_code == 200
    assert response.json() == {"filename": "jokes.json"}


def test_check_save_folder():
    """Test how to api knows check files in folder"""
    response = client.get("files/savefolder")
    assert response.status_code == 200



def test_read_file_from_save():
    response = client.get("files/savefolder/?file_id=1")
    assert response.status_code == 200
    assert response.json() == "hello"


def test_get_all_jokes():
    response = client.get("/jokes/alljokes")
    assert response.status_code == 200

def test_return_specific_joke():
    response = client.get("/jokes/?joke_id=0")
    assert response.status_code == 200
    assert {
          "id": 0,
          "text": "q. How do you comfort a JavaScript bug? a. You console it.",
          "question": "How do you comfort a JavaScript bug?",
          "answer": "You console it.",
          "author": "elijahmanor",
          "created": "09/06/2013",
          "tags": [
            "javascript"
          ],
          "rating": 1
        } == response.json()


def test_input_joke():
    response = client.post("jokes/insert/", json={
                                                  "text": "string",
                                                  "question": "string",
                                                  "answer": "string",
                                                  "author": "string",
                                                  "created": "string",
                                                  "tags": [
                                                    "test"
                                                  ],
                                                  "rating": 0
                                                })
    response.status_code == 200
    response.json() == "Joke was inserted under id: 5"

def test_delete_joke():
    response = client.post('/jokes/delete/?joke_id=5')
    response.status_code == 200
    response.json() == "Joke id: 5 was deleted"