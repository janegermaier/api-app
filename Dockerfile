FROM ubuntu

RUN apt-get update && apt-get install -y \
    software-properties-common
RUN add-apt-repository universe
RUN apt-get update && apt-get install -y \
    python3.7 \
    python3-pip

EXPOSE 8000

COPY . .

RUN pip3 install -r requirements.txt

ENTRYPOINT ["uvicorn", "api.main:app", "--host", "0.0.0.0", "--port", "8000"]
